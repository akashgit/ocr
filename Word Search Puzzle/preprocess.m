function letters = preprocess(grid)

%split the image into 30x30 squares each representing one letter
dimensions = 30; % pixels
[x, y] = size(grid);
count = 0;

for row = 1:dimensions:x

    for cell = 1:dimensions:y
       
        count = count + 1;
        %get next 30x30 chunk as 1 by 900 features of current letter
        letters(count, :) = reshape(grid(row:(row+dimensions-1),...
        cell:(cell+dimensions-1)), 1, 900);
        
    end
    
end

end