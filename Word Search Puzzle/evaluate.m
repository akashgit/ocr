function similarity = evaluate(word, test)

correct = 0; %how many matched chars have been found
word = upper(word);
test = upper(test);

%go through all characters one-by-one
for i = 1:length(word)
  
    %if current char matches the corresponding char in the tested word
    if(word(i) == test(i))
        correct = correct + 1;
    end
    
end

%get the match in percentage
similarity = (correct / length(word)) * 100;
    
end