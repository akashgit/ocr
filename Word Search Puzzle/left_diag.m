function d = left_diag(grid)

[x, y] = size(grid);

for i = 1:x
	d(i) = grid(x-(i-1), i);
end

end