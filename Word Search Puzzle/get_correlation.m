function c = get_correlation(train_data)

corr = sum(corrcoef(train_data));
[sorted, indexes] = sort(corr);

c = indexes(1:50);

end