function sim = similar(a, b)

%similar characters being mistaken
chars = [
    'C', 'O';
    'T', 'Y';
    'I', 'J';
    'U', 'V';
    'N', 'H';
    'A', 'R'
];

[x, len] = size(chars);
sim = 0;

for i = 1:x
    
    row = chars(i, :);
    if(row(row==a))
        
        for j = 1:len
            if(row(j) == b)
                sim = 1; return;
            else
                sim = 0;
            end
        end
        
    elseif(row(row==b))
        
        for j = 1:len
            if(row(j) == a)
                sim = 1; return;
            else
                sim = 0;
            end
        end
    end
    
end

end