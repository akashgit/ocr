function [pos, loc] = check_axes(grid, current_location)

%get all possible letter combinations on both vertical and horizontal axes
[x, len] = size(grid);
current_location = current_location - 1;

pos(1, :) = grid(1, :); %possible word
loc(1, :) = struct('start', [1, 1] + current_location, 'end',  [1, len] + current_location); %location of first and last letter
pos(2, :) = fliplr(grid(1, :));
loc(2, :) = struct('start', [1, len] + current_location, 'end', [1, 1] + current_location);

pos(3, :) = reshape(grid(:, 1), 1, len);
loc(3, :) = struct('start', [1, 1] + current_location, 'end', [len, 1] + current_location);
pos(4, :) = fliplr(reshape(grid(:, 1), 1, len));
loc(4, :) = struct('start', [len, 1] + current_location, 'end', [1, 1] + current_location);

end