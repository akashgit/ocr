function test_labels = classify(train, train_labels, test)

% Nearest neighbour classification.

% Super compact implementation of nearest neighbour 
x= test * train';
modtest=sqrt(sum(test.*test,2));
modtrain=sqrt(sum(train.*train,2));
dist = x./(modtest*modtrain'); % cosine distance
[d,i]=max(dist');
test_labels = train_labels(i);


