function red_data = red_train_data(train_data, features)

red_data = train_data(:, features);

end