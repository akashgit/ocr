function d = get_divergences(train_data, train_labels)

let_num = 26;
divergences = [1:size(train_data, 2)];

for i = 1:let_num
   
    letter_data(i, 1) = struct('features', train_data(train_labels==i, :));
    
end

%get divergence between evey pair of letters
for i = 1:let_num
   
   for j = 1:let_num
       
       data1 = letter_data(i).features;
       data2 = letter_data(j).features;
       
       %sum divergences
       divergences = divergences + divergence(data1, data2);
       
   end
   
end

%get the 40 highest divergences
[sorted, indexes] = sort(-divergences);
d = indexes(1:40);

end