function [pctrain_data, pctest_data] = reduce_dim(train_data, test_data, train_labels)

covx=cov(train_data);
[v,d]=eigs(covx,40);

pctrain_data = train_data * v;
pctest_data = test_data * v;

best = get_divergences(pctrain_data, train_labels);

pctrain_data = pctrain_data(:, best10);
pctest_data = pctest_data(:, best10);

end