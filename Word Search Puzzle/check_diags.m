function [pos, loc] = check_diags(grid, current_location)

%get all possible letter combinations on both diagonals
[x, len] = size(grid);
current_location = current_location - 1;

pos(1, :) = reshape(diag(grid), 1, len); %possible word
loc(1, :) = struct('start', [1, 1] + current_location, 'end',  [len, len] + current_location); %location of first and last letter
pos(2, :) = fliplr(reshape(diag(grid), 1, len));
loc(2, :) = struct('start', [len, len] + current_location, 'end', [1, 1] + current_location);

pos(3, :) = reshape(left_diag(grid), 1, len);
loc(3, :) = struct('start', [len, 1] + current_location, 'end', [1, len] + current_location);
pos(4, :) = fliplr(reshape(left_diag(grid), 1, len));
loc(4, :) = struct('start', [1, len] + current_location, 'end', [len, 1] + current_location);

end