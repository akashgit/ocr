function solution = wordsearch(test, words, train_data, train_labels)

characters = 'A':'Z';

%draw letter grid image and wait for lines to be drawn
imagesc(test);
colormap(gray);
hold on;

%extract individual letters from grid image
letter_features = preprocess(test);

apply_dim_red = true;

%--------- dimensionality reduction applied ------
if(apply_dim_red)
   [train_data, letter_features] = reduce_dim(train_data, letter_features, train_labels);
end
%-------------------------------------------------

%classify letters from letter images
letter_labels = reshape(classify(train_data, train_labels, letter_features), 15, 15)';
chars = characters(letter_labels); %grid of classified letters

%for each word to find
for i = 1:length(words)
    %find next word
    next = find_next(char(words(i)), chars);
    draw_line(next);
    
end

hold off;

end