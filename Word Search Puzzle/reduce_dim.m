function [reduced_train, reduced_test] = reduce_dim(train_data, test_data, train_labels)

[v, d] = eigs(cov(train_data), 40);

train_data = train_data * v;
test_data = test_data * v;

%select best ten features
features = get_divergences(train_data, train_labels);

%get reduced test and data sets
reduced_train = train_data(:, features);
reduced_test = test_data(:, features);

end