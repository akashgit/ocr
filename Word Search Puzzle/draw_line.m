function line = draw_line(position)

    line = plot([position.start(2), position.end(2)] * 30 - 15, [position.start(1), position.end(1)] * 30 - 15);
    set(line, 'Color', 'yellow', 'LineWidth', 3);

end