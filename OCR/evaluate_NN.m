function correct = evaluate_NN(test, train)

    % k gives us number of values in each feature vector
    % l is never used
    [l, k] = size(test);
    countCorrect = 0; % number of correctly classified letters
    for i = 1:k
       
        foundLabel = classify_NN(test(i).fvector, train); % guessed label of next letter
        correctLabel = test(i).label; % actual label of next letter
        
        if(foundLabel == correctLabel)
           countCorrect = countCorrect + 1; % label has been classified correctly
        end
        
    end
    
    % percentage value of correctly classified letters
    correct = (countCorrect / k) * 100;

end