function plotData = experiment(maxNoise, test, train)

    resolution = 10; % number of steps on x axis
    step = maxNoise / resolution; % noise from 0 - maxNoise in resolution-times steps
    
    for i = 1:resolution % for each step we need to introduce gradually increased noise
        noise = normrnd(0, sqrt(step*i), 900, 1); % increased noise
        for j = 1:225
            test(j).fvector = test(j).fvector + noise; % apply noise to each feature vector in test data
        end
        
        variance(i) = sqrt(step*i); % variance values on x-axis
        plotData(i) = evaluate_NN(test, train); % evaluation of test data after noise has been applied
                                                % values on y-axis
    end
    
    % plot settings
    plot(plotData); % plot is displayed and data returned
    set(gca,'YTick', 0:5:100)
    set(gca,'XTickLabel',variance.^2)
    xlabel('variance')
    ylabel('performance (%)')
    
end