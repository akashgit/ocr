function d = distance_euclidean(fvector1, fvector2)

    % sum of distances of both feature vectors
    d = sqrt(sum(abs((fvector1.^2)-(fvector2.^2))));

end