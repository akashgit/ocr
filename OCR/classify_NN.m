function class = classify_NN(fvector, train)

    % k gives us number of values in each feature vector
    % l is never used
    [l, k] = size(train);
 
    for i = 1:k
        distances(i,:) = [distance_euclidean(fvector, train(i).fvector), i]; % add distance of next pair (test-train) of veatures and index
    end
    
    sorted = sortrows(distances, 1); % sort distances row-wise moving the indexes with corresponding rows
    class = train(sorted(1,2)).label; % display the label from train data
                                      % use index of the smallest distance

end